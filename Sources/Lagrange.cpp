/*
 */

#include "Lagrange.hpp"

Lagrange::Lagrange() {
}

Lagrange::Lagrange(const Lagrange& orig) {
    mXval = orig.mXval;
    mYval = orig.mYval;
    mCoef = orig.mCoef;
    mCoefLength = orig.mCoefLength;
    mVLength = orig.mVLength;
}

Lagrange::~Lagrange() {
}

void Lagrange::Solve() {
    mCoefLength = mVLength;
    mCoef = new double[mVLength];
    mCoef = FindCoef(mXval, mYval, mVLength, mVLength);
    CorrectLowCoef();
}