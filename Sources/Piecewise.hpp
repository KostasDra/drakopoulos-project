/* 
 * File:   Piecewise.hpp
 * Author: kostas
 *
 * Created on November 23, 2014, 7:25 PM
 */

#ifndef PIECEWISE_HPP
#define	PIECEWISE_HPP

#include "Approx.hpp"

//! Piecewise

//! Class to compute the Piecewise interpolation coefficients for splines.

class Piecewise : public Approx {
public:
    //! Default constructor. Empty.
    Piecewise();
    //! Copy constructor.
    Piecewise(const Piecewise& orig);
    //! Default destructor. Empty.
    virtual ~Piecewise();
    //! Method to produce the coefficients using splines.
    void Solve();
    //! Method to evaluate given x-values at the nodes. NOT COMPLETED.
    void Evaluate();
private:

};

#endif	/* PIECEWISE_HPP */

