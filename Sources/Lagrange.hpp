/* 
 * File:   Lagrange.hpp
 * Author: kostas
 *
 * Created on November 23, 2014, 7:23 PM
 */

#ifndef LAGRANGE_HPP
#define	LAGRANGE_HPP

#include "LeastSquares.hpp"

//! Lagrange

//! Find the Lagrange interpolation for the given values. This has been organised as a special case of the LeastSquares approximation (cols=rows).

class Lagrange : public LeastSquares {
private:

public:
    //! Default constructor. Empty.
    Lagrange();
    //! Copy constructor.
    Lagrange(const Lagrange& orig);
    //! Default destructor. Empty.
    ~Lagrange();
    //! Function to find the coefficients.
    void Solve();
};


#endif	/* LAGRANGE_HPP */

