/* 
 * File:   STest1.cpp
 * Author: kostas
 *
 * Created on Nov 26, 2014, 3:39:36 PM
 */

#include <stdlib.h>
#include <iostream>
#include "Fourier.hpp"
#include "Piecewise.hpp"
#include "Lagrange.hpp"

void test1() {
    std::cout << "STest1 test 1" << std::endl;
    Approx* poly = new LeastSquares;
    poly->SetPoints(0, 45, 10);
    poly->Solve();
    poly->Print();
    poly->WriteOut();
}

void test2() {
    std::cout << "STest1 test 2" << std::endl;
    Approx* poly = new Lagrange;
    poly->SetPoints(0, 45, 10);
    poly->Solve();
    poly->Print();
    poly->WriteOut();
    std::cout << "%TEST_FAILED% time=0 testname=test2 (STest1) message=error message sample" << std::endl;
}

void test3() {
    std::cout << "STest1 test 3" << std::endl;
    Approx* poly = new Piecewise;
    poly->SetPoints(0, 45, 10);
    poly->Solve();
    poly->Print();
    poly->WriteOut();
}

void test4() {
    std::cout << "STest1 test 4" << std::endl;
    Approx* poly = new Fourier;
    poly->SetPoints(0, 45, 10);
    poly->Solve();
    poly->Print();
    poly->WriteOut();
}

int main(int argc, char** argv) {
    std::cout << "%SUITE_STARTED%" << std::endl;

    std::cout << "%TEST_STARTED% Test 1: Least Squares (Test)" << std::endl;
    test1();
    std::cout << "%TEST_FINISHED% Test 1: Least Squares" << std::endl;

    std::cout << "%TEST_STARTED% Test 2: Lagrange (Test)\n" << std::endl;
    test2();
    std::cout << "%TEST_FINISHED% Test 2: Lagrange" << std::endl;

    std::cout << "%TEST_STARTED% Test 3: Piecewise (Test)\n" << std::endl;
    test3();
    std::cout << "%TEST_FINISHED% Test 3: Piecewise" << std::endl;

    std::cout << "%TEST_STARTED% Test 4: Fourier (Test)\n" << std::endl;
    test4();
    std::cout << "%TEST_FINISHED% Test 4: Fourier" << std::endl;

    std::cout << "\n %SUITE_FINISHED%" << std::endl;

    return (EXIT_SUCCESS);
}

