/*
 *
 */

#include <fstream>
#include <cmath>
#include <cstdlib>
#include "Approx.hpp"

// Constructor. If no file name is specified, look for input.dat

Approx::Approx() {
}

Approx::Approx(string inxFile, string inxV, string inyFile, string inyV) {
    SetPoints(inxFile, inxV);
    SetPoints(inyFile, inyV);
}

Approx::Approx(double* Xval, double* Yval) {
    mXval = Xval;
    mYval = Yval;
}

Approx::~Approx() {
    delete[] mXval;
    delete[] mYval;
    delete[] mCoef;
}

// Read the points from file
void Approx::SetInputLength(int length){
    mVLength=length;
}

void Approx::SetOutputLength(int length){
    mOLength=length;
}

void Approx::SetPoints(string inFile, string inV) {
    ifstream readFile(inFile.c_str());
    assert(readFile.is_open());

    if (inV == "x" or "X") {
        mXval = new double[mVLength];
    } else if (inV == "y" or "Y") {
        mYval = new double[mVLength];
    } else {
        cout << "Please specify where the input vector should be stored " <<
                "by using 'x' for the x-values and 'y' for the y-values" << endl;
    }

    int i = 0;
    while (!readFile.eof()) {
        if (inV == "x" or "X") {
            readFile >> mXval[i];
        }
        if (inV == "y" or "Y") {
            readFile >> mYval[i];
        }
        i++;
    }
    readFile.close();
}

// Convert a given function to an interpolated version.

/* Does not work. I could not make it work.
void Approx::SetPointsC(double bound_low, double bound_high, int pointNo, double (*convertFunction)(double)) {
    for (int i = 0; i < pointNo; i++) {
        mXval[i] = (bound_low + bound_high) / 2 - (bound_high - bound_low) / 2 * cos(pi * i / (pointNo - 1));
        mYval[i] = (*convertFunction)(mXval[i]);
    }
}
 */
void Approx::SetPoints(double bound_low, double bound_high, int pointNo) {
    mXval = new double[pointNo];
    mYval = new double[pointNo];
    for (int i = 0; i < pointNo; i++) {
        mXval[i] = (bound_high - bound_low) / ((double) (pointNo - 1)) * i;
        mYval[i] = mXval[i] * mXval[i] + 5 * mXval[i]; // - (rand() % 10);
        cout << mXval[i] << "   " << mYval[i] << "\n";
    }
    mVLength = pointNo;
}

double Approx::GetX() const {
    return* mXval;
}

double Approx::GetY() const {
    return* mYval;
}

double Approx::GetCoef() const {
    return* mCoef;
}

int Approx::GetLength() const {
    return mVLength;
}

// Write the coefficients to file as a column vector

void Approx::WriteOut() {
    ofstream writeOutput("coefficients.dat");
    for (int i = 0; i < mCoefLength; i++) {
        writeOutput << mCoef[i] << "\n";
    }
    writeOutput.close();
}

void Approx::Print() {
    for (int i = 0; i < mCoefLength; i++) {
        std::cout << mCoef[i] << "\n";
    }
}

void Approx::CorrectLowCoef() {
    double max = 0;
    for (int i = 0; i < mCoefLength; i++) {
        if (abs(mCoef[i]) > max) {
            max = mCoef[i];
        }
    }
    for (int i = 0; i < mCoefLength; i++) {
        if (abs(mCoef[i] / max) < 1e-3) {
            mCoef[i] = 0.0;
        }
    }
}