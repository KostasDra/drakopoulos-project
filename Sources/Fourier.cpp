/* 
 * File:   Fourier.cpp
 * Author: kostas
 * 
 * Created on November 23, 2014, 7:29 PM
 */

#include "Fourier.hpp"

Fourier::Fourier() {
}

Fourier::Fourier(const Fourier& orig) {
}

Fourier::~Fourier() {
}

void Fourier::Solve() {
    double l;
    double h = 2 * pi / mVLength;
    int miu = (mVLength - 1) % 2;
    int M = (mVLength - 1) / 2;
    mCoefLength = 2 * mVLength;
    mCoef = new double [mCoefLength];

    double* coefR;
    coefR = new double[2 * (M + miu) + 1];
    double* coefI;
    coefI = new double[2 * (M + miu) + 1];

    for (int k = 0; k < 2 * (M + miu); k++) {
        l = k - M - miu;
        for (int j = 0; j < mVLength; j++) {
            coefR[k] += 1 / ((double) mVLength) * mYval[j] * cos(-l * j * h);
            coefI[k] += 1 / ((double) mVLength) * mYval[j] * sin(-l * j * h);
        }
    }
    if (miu != 0) {
        coefR[2 * (M + miu)] = coefR[0];
        coefI[2 * (M + miu)] = coefI[0];
    }

    int i = 0;
    for (int k = 0; k < 2 * (M + miu); k += 2) {
        mCoef[2 * k] = coefR[i] + coefR[2 * (M + miu) - i];
        mCoef[2 * k + 1] = -coefI[i] + coefI[2 * (M + miu) - i];
        i++;
    }

    CorrectLowCoef();
}

void Fourier::Evaluate() {
}