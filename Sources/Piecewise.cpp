/* 
 * File:   Piecewise.cpp
 * Author: kostas
 * 
 * Created on November 23, 2014, 7:25 PM
 */

#include "Piecewise.hpp"

Piecewise::Piecewise() {
}

Piecewise::Piecewise(const Piecewise& orig) {
    mXval = orig.mXval;
    mYval = orig.mYval;
    mCoef = orig.mCoef;
    mCoefLength = orig.mCoefLength;
    mVLength = orig.mVLength;
}

Piecewise::~Piecewise() {
}

void Piecewise::Solve() {
    MatrixXd A(mVLength, mVLength);
    VectorXd y(mVLength);
    VectorXd sol(mVLength);
    mCoef = new double [mVLength];

    // Set the values for the first and last row for A and y
    A(0, 0) = 2 / (mXval[1] - mXval[0]);
    A(0, 1) = 1 / (mXval[1] - mXval[0]);
    A(mVLength - 1, mVLength - 2) = 1 / (mXval[mVLength - 1] - mXval[mVLength - 2]);
    A(mVLength - 1, mVLength - 1) = 2 / (mXval[mVLength - 1] - mXval[mVLength - 2]);

    y(0) = 3 * (mYval[1] - mYval[0]) / ((mXval[1] - mXval[0])*(mXval[1] - mXval[0]));
    y(mVLength - 1) = 3 * (mYval[mVLength - 1] - mYval[mVLength - 2]) /
            ((mXval[mVLength - 1] - mXval[mVLength - 2])*
            (mXval[mVLength - 1] - mXval[mVLength - 2]));

    // Fill first/last rows with zeros except the first two/last two elements
    for (int i = 0; i < mVLength; i++) {
        if (i > 1) {
            A(0, i) = 0;
        }
        if (i < mVLength - 2) {
            A(mVLength - 1, i) = 0;
        }
    }

    // Populate the matrix
    for (int i = 1; i < mVLength - 1; i++) {
        y(i) = 3 * ((mYval[i + 1] - mYval[i]) / ((mXval[i + 1] - mXval[i])*(mXval[i + 1] - mXval[i]))+
                (mYval[i + 2] - mYval[i + 1]) / ((mXval[i + 2] - mXval[i + 1])*(mXval[i + 2] - mXval[i + 1])));
        for (int j = 0; j < mVLength; j++) {

            if (i - 1 == j) {
                A(i, j) = 1 / (mXval[i + 1] - mXval[i]);

            } else if (i == j) {
                A(i, j) = 2 / (mXval[i + 1] - mXval[i]) + 2 / (mXval[i + 2] - mXval[i + 1]);

            } else if (i + 1 == j) {
                A(i, j) = 1 / (mXval[i + 2] - mXval[i + 1]);

            } else {
                A(i, j) = 0;
            }
        }
    }
    sol = A.fullPivLu().solve(y);
    mCoefLength = mVLength;
    for (int i = 0; i < mVLength; i++) {
        mCoef[i] = sol(i);
    }
    CorrectLowCoef();
}

void Piecewise::Evaluate() {
}