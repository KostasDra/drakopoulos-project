/* 
 * Obtain the coefficients for a Least Squares Approximation.
 * File:   LeastSquare.hpp
 * Author: kostas
 *
 * Created on November 23, 2014, 7:28 PM
 */

#ifndef LEASTSQUARES_HPP
#define	LEASTSQUARES_HPP

#include "Approx.hpp"

//! LeastSquares

//! Finds the LeastSquares approximation to the given data.

class LeastSquares: public Approx {
public:
     //! Default Constructor. Empty.
    LeastSquares();
     //! Copy Constructor.
    LeastSquares(const LeastSquares& orig);
    //! Default destructor. Empty.
    virtual ~LeastSquares();
    
    //! Function to create the 'Eigen' matrix/vector.
    MatrixXd MakeMat(double* xval, int length, int polyDeg);
    VectorXd MakeVect(double* yval, int length);
    //! Function that uses two input vectors (x and y) to find the coefficients according to the polynomial degree required. For polyDeg=length, the Lagrange approximation is produced. This function is inherited in Lagrange.cpp
    double* FindCoef(double* xval, double* yval, int length, int polyDeg);
     //! Find the coefficients.
    virtual void Solve();
    //! Evaluate given x values at the given coefficients.
    void Evaluate(); 
private:

};

#endif	/* LEASTSQUARE_HPP */

