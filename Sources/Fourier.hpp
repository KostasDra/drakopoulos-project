/* 
 * File:   Fourier.hpp
 * Author: kostas
 *
 * Created on November 23, 2014, 7:29 PM
 */

#ifndef FOURIER_HPP
#define	FOURIER_HPP

#include "Approx.hpp"

//! Fourier

//! Function to produce the Fourier transform in O(N^2)
//! The results are different from what Matlab produced. After trying other algorithms, this one produced the least errors. There is something that I did not understand properly.

class Fourier : public Approx {
public:
    //! Default constructor
    Fourier();
    //! Copy constructor
    Fourier(const Fourier& orig);
    //! Default destructor
    virtual ~Fourier();
    //! Find coefficients
    void Solve();
    //! Method to evaluate given x-values at the nodes. NOT COMPLETED.
    void Evaluate();
private:

};

#endif	/* FOURIER_HPP */

