/* 
 * File:   LeastSquare.cpp
 * Author: kostas
 * 
 * Created on November 23, 2014, 7:28 PM
 */

#include "LeastSquares.hpp"

LeastSquares::LeastSquares() {
    // Constructor

}

LeastSquares::LeastSquares(const LeastSquares& orig) {
    mXval = orig.mXval;
    mYval = orig.mYval;
    mCoef = orig.mCoef;
    mCoefLength = orig.mCoefLength;
    mVLength = orig.mVLength;
}

LeastSquares::~LeastSquares() {
}

double* LeastSquares::FindCoef(double* xval, double* yval, int length, int polyDeg) {
    MatrixXd A(length, polyDeg);
    A = MakeMat(xval, length, polyDeg);
    VectorXd Y(length);
    Y = MakeVect(yval, length);
    VectorXd sol(polyDeg);
    double* coef;
    coef = new double [polyDeg];

    if (length == polyDeg) {
        sol = A.fullPivLu().solve(Y);
    } else {
        MatrixXd LHS = A.transpose() * A;
        MatrixXd RHS = A.transpose() * Y;

        sol = LHS.fullPivLu().solve(RHS);
    }
    for (int i = 0; i < polyDeg; i++) {
        coef[i] = sol(i);
    }
    return coef;
}

MatrixXd LeastSquares::MakeMat(double* xval, int length, int polyDeg) {
    MatrixXd A(length, polyDeg);
    for (int i = 0; i < length; i++) {
        A(i, 0) = 1;
        for (int j = 1; j < polyDeg; j++) {
            A(i, j) = A(i, j - 1) * xval[i];
        }
    }
    return A;
}

VectorXd LeastSquares::MakeVect(double* yval, int length) {
    VectorXd Y(length);
    for (int i = 0; i < length; i++) {
        Y(i) = yval[i];
    }
    return Y;
}

void LeastSquares::Solve() {
    int polyDeg = 2;
    mCoefLength = polyDeg + 1;
    mCoef = new double[mCoefLength];
    mCoef = FindCoef(mXval, mYval, mVLength, mCoefLength);
    CorrectLowCoef();
}

void LeastSquares::Evaluate() {
    MatrixXd A(mOLength, mCoefLength);
    VectorXd c(mOLength);
    VectorXd y(mOLength);
    A = MakeMat(mXout, mOLength, mCoefLength);
    c = MakeVect(mCoef, mOLength);
    y = A*c;
    for (int i = 0; i < mOLength; i++) {
        mYout[i] = y(i);
    }
}