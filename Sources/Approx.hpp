/** @mainpage Data Approximation library
 * @par This library approximates functions and data using the following methods:
 * - LeastSquares
 * - Lagrange
 * - Piecewise
 * - Fourier
 * 
 * @file Approx.cpp
 * Data approximation main file
 * This file contains the main methods inherited by other functions.
 * It establishes the inputs and creates the outputs.
 * 
 * Created on: 13 Nov 2014
 *
 * Created by: Konstantinos Drakopoulos
 *
 */
/**
 * This function handles the inputs and outputs.
 * @param[in] mXval Vector containig the x-values.
 */

#ifndef APPROX_HPP
#define APPROX_HPP

#include <iostream>
#include <string>
#include <cmath>
#include <cassert>
#include <Eigen/Dense>

using namespace Eigen;
using namespace std;

//! Approx

//! Main class from which all others are derived

class Approx {
protected:
    //! Vector containing the input x-values.
    double* mXval;
    //! Vector containing the input y-values.
    double* mYval;
    //! Vector containing the coefficients.
    double* mCoef;
    //! Integer that keeps the input vector lengths (x and y).
    int mVLength;
    //! Integer that stores the coefficient vector length.
    int mCoefLength;

    //! Vector containing the x-values at which to evaluate y.
    double* mXout;
    //! mXout vector length
    int mOLength;
    //! Vector containing the y-values of the above.
    double* mYout;
public:
    string inputFile;
    //! Default constructor. Empty.
    Approx();
    //! Construct and call the SetPoints function for files.
    Approx(string inxFile, string inxV, string inyFile, string inyV);
    //! Construct and call the SetPoints function for the given vectors.
    Approx(double* Xval, double* Yval);
    // Destructor
    virtual ~Approx(); //! Default destructor: Empty.

    // Set methods
    //! Set vector lengths (i.e. #of data points)
    void SetInputLength(int length);
    void SetOutputLength(int length);

    //! Read the data from files. Use 'x' or 'y' in the second entry to read the file into the specified vector.
    virtual void SetPoints(string inFile, string inV);

    //! This function produces y=x^2 + 5x for the given bounds. For testing purposes only.
    virtual void SetPoints(double bound_low, double bound_high, int pointNo); //! Sets the vector values.

    // Get methods

    //! Method to get the vector length.
    int GetLength() const;

    //! Method to get the x vector.
    double GetX() const;

    //! Method to get the y vector.
    double GetY() const;

    //! Method to get the vector of coefficients.
    double GetCoef() const;

    // Find the coefficients and evaluate
    //! Virtual function to get the coefficient values. Empty here.
    virtual void Solve() = 0;
    //! Virtual function to get the y-values for the given x. Empty here.
    virtual void Evaluate() = 0;
    //! Method to set coefficient values that are close to 0, to 0.
    void CorrectLowCoef();

    // Output
    //! Method to write the coefficient vector to file.
    void WriteOut();
    //! Method to print to the console
    void Print();

    //! Define pi. The value from the cmath library presented problems.
    const double pi = 3.14159265359;
};

#endif
